import json
import os
import sys
import time
import requests


from webwhatsapi import WhatsAPIDriver




print("Environment", os.environ)
try:
    os.environ["SELENIUM"]
except KeyError:
    print("Please set the environment variable SELENIUM to Selenium URL")
    sys.exit(1)

##Save session on "/firefox_cache/localStorage.json".
##Create the directory "/firefox_cache", it's on .gitignore
##The "app" directory is internal to docker, it corresponds to the root of the project.
##The profile parameter requires a directory not a file.
profiledir = os.path.join(".", "firefox_cache")
if not os.path.exists(profiledir):
    os.makedirs(profiledir)

driver = WhatsAPIDriver(
    profile=profiledir, client="remote", command_executor=os.environ["SELENIUM"]
)
print("Waiting for QR")
driver.wait_for_login()
print("Saving session")
driver.save_firefox_profile(remove_old=False)
print("Bot started")

target = [{'phone':'6282229452159','nama':'Indah'},{'phone':'6282135875088','nama':'Indira'}]

for kontak in target:
    time.sleep(3)

    phone_safe = kontak['phone']  # Phone number with country code
    phone_whatsapp = "{}@c.us".format(phone_safe)  # WhatsApp Chat ID
    image_path = "sample/1604555645.png"  # Local path file
    caption = ""  # The caption sent under the image, optional
    pesani = "ATTENTION Stay safe and healthy everyone🤗 \n ----‐-----------------------------✨✨---------------------------------- \n WORKSHOP START UI/UX WITH FIGMA 2020 \n-----------------------------------✨✨---------------------------------\n 🗣️ Pembicara : \n• Irfan Kurniawan (UI/ UX Designer at UI/UX Koding Teknologi Asia Udacoding) \n 🗣 Moderator\n• Danis Ardani\n🌸Pelaksanaan Kegiatan🌸\n📆 Minggu, 29 November – Minggu, 06 Desember 2020 \n⏰ 09.00 WIB - selesai\n📌 Zoom\n🌸Fasilitas🌸\n💡 Ilmu yang bermanfaat\n💡 Materi & Notulensi Webinar\n💡 Relasi\n💡 E-sertifikat\n📥Registrasi\nhttps://event.fostiums.org/workshop-start-uiux-with-figma-2020\n💵 HTM : 30K\nTransfer Rekening : \n0016 0108 6206 504 ( BRI ) \n089670951464 ( DANA, OVO, GOPAY ) \na.n Shofiya Nafisah \n𝙉𝙗: 𝙋𝙚𝙨𝙚𝙧𝙩𝙖 𝙮𝙖𝙣𝙜 𝙢𝙚𝙣𝙜𝙞𝙠𝙪𝙩𝙞 𝙖𝙘𝙖𝙧𝙖 𝙙𝙖𝙧𝙞 𝙖𝙬𝙖𝙡 𝙝𝙞𝙣𝙜𝙜𝙖 𝙖𝙠𝙝𝙞𝙧 𝙖𝙠𝙖𝙣 𝙢𝙚𝙣𝙚𝙧𝙞𝙢𝙖 𝙥𝙚𝙣𝙜𝙚𝙢𝙗𝙖𝙡𝙞𝙖𝙣 𝙙𝙖𝙣𝙖 65 %\n*Untuk Umum dan Slot Terbatas \n*Konfirmasi pembayaran ke CP\n📞 Untuk pendaftaran dan Info lebih lanjut, bisa hubungi 👇\nhttp://wa.me/6282138644681 ( Shofiya )\nhttp://wa.me/6289637711345 ( Nila )\nhttp://wa.me/6282229452159 ( Indah )\nStay safe and healthy everyone"
    
    driver.send_media(image_path, phone_whatsapp, str(pesani))
    
    print("Media file was successfully sent to {}".format(phone_safe))
